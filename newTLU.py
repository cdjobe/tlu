# Carson Jobe
# Artificial Intelligence - PA 1 - TLU ::: REWRITE

''' from assignment:
        Both programs will use command line parameters to obtain the file names:
                tluTrain trainFile validationFile modelFile
                tluEval testFile modelFile
                
        where trainFile, validationFile, and testFile are data files containing
        the training, validation, and testing partitions of data. The modelFile
        is created by tluTrain and should hold the weights and other information
        such that tluEval can recreate the TLUs from the file.

        2nd part? Maybe.

'''

import numpy as np
import sys
import random

# Generate Weights
def CreateWeights(input_array):

        # weight_width = dimensionality of each sample. 
        weight_height, weight_width = input_array.shape
#        print(input_array.shape)

        ## MIGHT HAVE TO ADJUST...Incorrect dimensions and all that ##
        weights =  np.zeros((weight_height, weight_width))
#        print(weights)
        
        bounds = 1/np.sqrt(1 + weight_width)
        
        for i in range(weight_height):
                for j in range(weight_width):
                        weights[i, j] = random.uniform(-bounds, bounds)
        
        return weights
# End GenerateWeights


# Get Class and Train data
def SeparateData(input_array):

        height, width = input_array.shape
        class_data = np.zeros((height,1))
        train_data = np.zeros((height, width))

        for i in range(height):

                class_data[i] = input_array[i, 0]

                for j in range(width):

                        train_data[i, j] = input_array[i, j]
                        train_data[i, 0] = 1    # Biased 1

#        print(class_data)
        print(train_data)

        return class_data, train_data
# End SeparateData

# LearningRate
# a = 1/(10 + t^2) where t = epoch
    
def LearningRate(epoch):
    
    alpha = 1 / (10 + epoch**2)
    return alpha
# End LearningRate
    

# Squash
# y = 1 if SUM(Xi * Wi) + (-1 * theta) >= 0
def Squash(a):

    if (a >= 0):
        y = 1
    else:
        y = 0
    return y
# End Squash

# ModifyWeights
''' w1 = w0 + a(d - y)x
    where:
    w0 = current weight
    a = learning rate
    d = desired output
    y = actual output
    x = input
'''
def ModifyWeights():

# End ModifyWeights

# Train
'''
http://stackoverflow.com/questions/2976452/whats-is-the-difference-between-train-validation-and-test-set-in-neural-networ

for each epoch
    for each training data instance
        propagate error through the network
        adjust the weights
        calculate the accuracy over training data
    for each validation data instance
        calculate the accuracy over the validation data
    if the threshold validation accuracy is met
        exit training
    else
        continue training
'''

def Train(train_input, valid_input, weights):

        height, width = train_input.shape
        fully_trained = False
        class_index = 0 # will be used for the index of the class array/table
        
        classes, samples = SeparateData(train_input)

        print(samples.shape)
        print(weights.shape)

        while(fully_trained != True):

                fully_trained = True
                for sample in samples:
                        for weight in weights:
                                
                                #print(sample.shape, weight.shape)
                                # A = (X1 * W1) + (X2 * W2) +...+ (Xi * Wi)....
                                activation = np.dot(sample, weight) # Dot product of sample and weight
#                                print(a)
                                a = activation - Threshold(activation)
#                                print(a)
                                y_output = Squash(a)
                                alpha = LearningRate(epoch)
                                ModifyWeights(weight, alpha, classes[class_index], y_output, sample)
                                class_index += 1 # update height_index for next pass for classes
# End Train                       


############################################################
# Small Stuff ##############################################
############################################################
# Create Threshold
def Threshold(input_data):
        # input_data = x*w
        theta = (1/(1+np.exp(-input_data)))
        return theta
# End Threshold

# Learning Rate
def LearningRate(epoch):
      a = 1/(10 + epoch*epoch)
      return a          
# End Learning Rate        

# Convert open files to arrays
def ConvertToArray(input_file):
        output_array = np.loadtxt(input_file)
        return output_array
# End ConvertToArray        
############################################################
# End Small Stuff ##########################################
############################################################

# Main
def main():
        random.seed(1)

#        Save argument names as strings
#        train_data = sys.argv[1]
#        validation_data = sys.argv[2]
#        model_data = sys.argv[3]
        
        ''' Files:
                    irisTest.txt
                    irisTrain.txt
                    irisValidation.txt
        '''
        
        train_data = open("irisTest.txt")
        validation_data = open("irisValidation.txt")

        # Convert the open files to arrays
        train_data = ConvertToArray(train_data)
        validation_data = ConvertToArray(validation_data)
        
        weights = CreateWeights(train_data)

        Train(train_data, validation_data, weights)

# End main
        
##########
main()